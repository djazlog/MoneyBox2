// @generated
//  This file was automatically generated and should not be edited.

import Apollo
import Foundation

public final class LoginMutation: GraphQLMutation {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    mutation Login($phone: String!, $password: String!) {
      login(phone: $phone, password: $password)
    }
    """

  public let operationName: String = "Login"

  public var phone: String
  public var password: String

  public init(phone: String, password: String) {
    self.phone = phone
    self.password = password
  }

  public var variables: GraphQLMap? {
    return ["phone": phone, "password": password]
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Mutation"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("login", arguments: ["phone": GraphQLVariable("phone"), "password": GraphQLVariable("password")], type: .nonNull(.scalar(String.self))),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(login: String) {
      self.init(unsafeResultMap: ["__typename": "Mutation", "login": login])
    }

    /// Авторизация
    public var login: String {
      get {
        return resultMap["login"]! as! String
      }
      set {
        resultMap.updateValue(newValue, forKey: "login")
      }
    }
  }
}

public final class ProfileQuery: GraphQLQuery {
  /// The raw GraphQL definition of this operation.
  public let operationDefinition: String =
    """
    query Profile {
      profile {
        __typename
        name
        phone
        balance
        setting {
          __typename
          type
          stock
          bonds
          gold
          currency
        }
        operations {
          __typename
          from
          amount
          action
        }
      }
    }
    """

  public let operationName: String = "Profile"

  public init() {
  }

  public struct Data: GraphQLSelectionSet {
    public static let possibleTypes: [String] = ["Query"]

    public static var selections: [GraphQLSelection] {
      return [
        GraphQLField("profile", type: .object(Profile.selections)),
      ]
    }

    public private(set) var resultMap: ResultMap

    public init(unsafeResultMap: ResultMap) {
      self.resultMap = unsafeResultMap
    }

    public init(profile: Profile? = nil) {
      self.init(unsafeResultMap: ["__typename": "Query", "profile": profile.flatMap { (value: Profile) -> ResultMap in value.resultMap }])
    }

    /// Получение данные о авторизированном пользователе
    public var profile: Profile? {
      get {
        return (resultMap["profile"] as? ResultMap).flatMap { Profile(unsafeResultMap: $0) }
      }
      set {
        resultMap.updateValue(newValue?.resultMap, forKey: "profile")
      }
    }

    public struct Profile: GraphQLSelectionSet {
      public static let possibleTypes: [String] = ["User"]

      public static var selections: [GraphQLSelection] {
        return [
          GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
          GraphQLField("name", type: .scalar(String.self)),
          GraphQLField("phone", type: .nonNull(.scalar(String.self))),
          GraphQLField("balance", type: .nonNull(.scalar(Double.self))),
          GraphQLField("setting", type: .object(Setting.selections)),
          GraphQLField("operations", type: .list(.nonNull(.object(Operation.selections)))),
        ]
      }

      public private(set) var resultMap: ResultMap

      public init(unsafeResultMap: ResultMap) {
        self.resultMap = unsafeResultMap
      }

      public init(name: String? = nil, phone: String, balance: Double, setting: Setting? = nil, operations: [Operation]? = nil) {
        self.init(unsafeResultMap: ["__typename": "User", "name": name, "phone": phone, "balance": balance, "setting": setting.flatMap { (value: Setting) -> ResultMap in value.resultMap }, "operations": operations.flatMap { (value: [Operation]) -> [ResultMap] in value.map { (value: Operation) -> ResultMap in value.resultMap } }])
      }

      public var __typename: String {
        get {
          return resultMap["__typename"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "__typename")
        }
      }

      public var name: String? {
        get {
          return resultMap["name"] as? String
        }
        set {
          resultMap.updateValue(newValue, forKey: "name")
        }
      }

      public var phone: String {
        get {
          return resultMap["phone"]! as! String
        }
        set {
          resultMap.updateValue(newValue, forKey: "phone")
        }
      }

      public var balance: Double {
        get {
          return resultMap["balance"]! as! Double
        }
        set {
          resultMap.updateValue(newValue, forKey: "balance")
        }
      }

      public var setting: Setting? {
        get {
          return (resultMap["setting"] as? ResultMap).flatMap { Setting(unsafeResultMap: $0) }
        }
        set {
          resultMap.updateValue(newValue?.resultMap, forKey: "setting")
        }
      }

      public var operations: [Operation]? {
        get {
          return (resultMap["operations"] as? [ResultMap]).flatMap { (value: [ResultMap]) -> [Operation] in value.map { (value: ResultMap) -> Operation in Operation(unsafeResultMap: value) } }
        }
        set {
          resultMap.updateValue(newValue.flatMap { (value: [Operation]) -> [ResultMap] in value.map { (value: Operation) -> ResultMap in value.resultMap } }, forKey: "operations")
        }
      }

      public struct Setting: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Setting"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("type", type: .nonNull(.scalar(String.self))),
            GraphQLField("stock", type: .nonNull(.scalar(Int.self))),
            GraphQLField("bonds", type: .nonNull(.scalar(Int.self))),
            GraphQLField("gold", type: .nonNull(.scalar(Int.self))),
            GraphQLField("currency", type: .nonNull(.scalar(Int.self))),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(type: String, stock: Int, bonds: Int, gold: Int, currency: Int) {
          self.init(unsafeResultMap: ["__typename": "Setting", "type": type, "stock": stock, "bonds": bonds, "gold": gold, "currency": currency])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        /// Копим через фонд или доверительное управление
        public var type: String {
          get {
            return resultMap["type"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "type")
          }
        }

        /// Процент акций в портфеле
        public var stock: Int {
          get {
            return resultMap["stock"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "stock")
          }
        }

        /// Процент облигаций в портфеле
        public var bonds: Int {
          get {
            return resultMap["bonds"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "bonds")
          }
        }

        /// Процент золото в портфеле
        public var gold: Int {
          get {
            return resultMap["gold"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "gold")
          }
        }

        /// Процент валюты в портфеле
        public var currency: Int {
          get {
            return resultMap["currency"]! as! Int
          }
          set {
            resultMap.updateValue(newValue, forKey: "currency")
          }
        }
      }

      public struct Operation: GraphQLSelectionSet {
        public static let possibleTypes: [String] = ["Operation"]

        public static var selections: [GraphQLSelection] {
          return [
            GraphQLField("__typename", type: .nonNull(.scalar(String.self))),
            GraphQLField("from", type: .scalar(String.self)),
            GraphQLField("amount", type: .scalar(Double.self)),
            GraphQLField("action", type: .scalar(String.self)),
          ]
        }

        public private(set) var resultMap: ResultMap

        public init(unsafeResultMap: ResultMap) {
          self.resultMap = unsafeResultMap
        }

        public init(from: String? = nil, amount: Double? = nil, action: String? = nil) {
          self.init(unsafeResultMap: ["__typename": "Operation", "from": from, "amount": amount, "action": action])
        }

        public var __typename: String {
          get {
            return resultMap["__typename"]! as! String
          }
          set {
            resultMap.updateValue(newValue, forKey: "__typename")
          }
        }

        public var from: String? {
          get {
            return resultMap["from"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "from")
          }
        }

        public var amount: Double? {
          get {
            return resultMap["amount"] as? Double
          }
          set {
            resultMap.updateValue(newValue, forKey: "amount")
          }
        }

        public var action: String? {
          get {
            return resultMap["action"] as? String
          }
          set {
            resultMap.updateValue(newValue, forKey: "action")
          }
        }
      }
    }
  }
}
