//
//  ShadowView.swift
//  MoneyBox
//
//  Created by Dmitriy Debelov on 21.04.2021.
//

import UIKit

class ShadowView: UIView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        setRadius()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setRadius()
    }
    
    func setRadius() {
        /*clipsToBounds = true
        layer.cornerRadius = 20
        layer.shadowRadius = 40
        layer.shadowOpacity = 1
        layer.shadowColor = UIColor.green.cgColor
        layer.shadowOffset = CGSize(width:30, height:30)
        */
        
        self.layer.cornerRadius = 20
        self.layer.shadowPath =
              UIBezierPath(roundedRect: self.bounds,
              cornerRadius: self.layer.cornerRadius).cgPath
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOpacity = 0.3
        self.layer.shadowOffset = CGSize(width: 0, height: 2)
        self.layer.shadowRadius = 10
        self.layer.masksToBounds = false
    }
}
