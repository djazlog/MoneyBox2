//
//  ProfileViewController.swift
//  MoneyBox
//
//  Created by Dmitriy Debelov on 20.04.2021.
//

import UIKit

class ProfileViewController: UIViewController {

    var profile:String = ""
    
    @IBOutlet weak var profileName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       // profileName.text = profile
        
        Network.shared.apollo.fetch(query: ProfileQuery()) { result in
          switch result {
          case .success(let graphQLResult):
            //print("Success! Result: \(graphQLResult)")
            let name = graphQLResult.data?.profile?.name ?? " null "
            self.profileName.text = "Hello \(name)"
          case .failure(let error):
            print("Failure! Error: \(error)")
          }
        }
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
