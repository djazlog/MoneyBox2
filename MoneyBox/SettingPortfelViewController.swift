//
//  SettingPortfelViewController.swift
//  MoneyBox
//
//  Created by Dmitriy Debelov on 18.04.2021.
//

import UIKit

class SettingPortfelViewController: UIViewController {

    var text:String = ""
    @IBOutlet weak var changer3: UISlider!
    @IBOutlet weak var changer2: UISlider!
    @IBOutlet weak var changer4: UISlider!
    
    @IBOutlet weak var changer1: UISlider!
    
    @IBOutlet weak var changerText1: UILabel!
    @IBOutlet weak var changerText2: UILabel!
    @IBOutlet weak var changerText3: UILabel!
    @IBOutlet weak var changerText4: UILabel!
    
    @IBOutlet weak var totalProcent: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

       //title = text
    }
    
    @IBAction func touchDrag(_ sender: UISlider) {
        let total:Float = 100
        switch sender {
            case changer1:
                let f1 = (sender.value + changer2.value)
                let f2 = (changer3.value + changer4.value)
                if  f1 + f2  >= total {
                    sender.value = total - (changer2.value + changer3.value + changer4.value)
                }
                changerText1.text = String(format: "%i",Int(changer1.value)) + "%"
                
            case changer2:
                let f1 = (changer1.value + sender.value)
                let f2 = (changer3.value + changer4.value)
                if  f1 + f2  >= total {
                    sender.value = total - (changer1.value + changer3.value + changer4.value)
                }
                changerText2.text = String(format: "%i",Int(changer2.value)) + "%"
            case changer3:
                    let f1 = (changer1.value + changer2.value)
                    let f2 = (sender.value + changer4.value)
                    if  f1 + f2  >= total {
                    sender.value = total - (changer1.value + changer2.value + changer4.value)
                    
                    }
                changerText3.text = String(format: "%i",Int(changer3.value)) + "%"
            default:
                    let f1 = (changer1.value + changer2.value)
                    let f2 = (changer3.value + sender.value)
                    if  f1 + f2  >= total {
                    sender.value = total - (changer1.value + changer2.value + changer3.value)
                    }
                changerText4.text = String(format: "%i",Int(changer4.value)) + "%"
        }
        
        let totalBal:Int = Int(changer1.value) + Int(changer2.value) + Int(changer3.value) + Int(changer4.value)
        totalProcent.text = String(format: "%i",Int(totalBal)) + "%"
    }

    
    @IBAction func changeActionSlider(_ sender: UISlider) {
        touchDrag(sender)
    }
    
    @IBAction func changeObligSlider(_ sender: UISlider) {
        touchDrag(sender)
    }
    
    
    @IBAction func changeGoldSlider(_ sender: UISlider) {
        touchDrag(sender)
    }
    
    
    @IBAction func changeValuteSlider(_ sender: UISlider) {
        touchDrag(sender)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
