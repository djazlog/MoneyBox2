//
//  ViewController.swift
//  MoneyBox
//
//  Created by Dmitriy Debelov on 17.04.2021.
//

import UIKit
import KeychainSwift

class ViewController: UIViewController {

    var usersList = ""
    
    @IBOutlet weak var globalBalanceText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        guard self.isLoggedIn() else {
          self.performSegue(withIdentifier: "showLogin", sender: self)
          return
        }
        
        gepProfile()
        
        
        

    }
    
    func gepProfile() {
        
        Network.shared.apollo.fetch(query: ProfileQuery()) { result in
          switch result {
          case .success(let graphQLResult):
            //print("Success! Result: \(graphQLResult)")
            if(graphQLResult.data?.profile == nil){
                self.goLoginPage()
            }
            
            let name = graphQLResult.data?.profile?.balance ?? 0
            self.globalBalanceText.text = "\(name) Р"
            
            
          case .failure(let error):
            self.goLoginPage()
            
            print("Failure! Login: \(error)")
            return

          }
        }
        
    }
    
    func goLoginPage(){
        let keychain = KeychainSwift()
        keychain.delete(LoginViewController.loginKeychainKey)
        
        self.performSegue(withIdentifier: "showLogin", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "goToSettingPortfel"){
            let newVC = segue.destination as! SettingPortfelViewController
            newVC.text = "testTest"
            
            Network.shared.apollo.fetch(query: ProfileQuery()) { result in
              switch result {
              case .success(let graphQLResult):
                print("Success! Result: \(graphQLResult)")
              case .failure(let error):
                print("Failure! Error: \(error)")
              }
            }
        }
        
        if(segue.identifier == "showProfile"){
            //let newVC = segue.destination as! ProfileViewController
            
        }
    }
    
    private func isLoggedIn() -> Bool {
      let keychain = KeychainSwift()
      return keychain.get(LoginViewController.loginKeychainKey) != nil
    }
    
   


}

