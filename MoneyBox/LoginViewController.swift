//
//  LoginViewController.swift
//  MoneyBox
//
//  Created by Dmitriy Debelov on 20.04.2021.
//

import UIKit
import KeychainSwift

class LoginViewController: UIViewController {

    static let loginKeychainKey = "login"
    
    
    @IBOutlet private var emailTextField: UITextField!
    @IBOutlet private var errorLabel: UILabel!
    @IBOutlet private var submitButton: UIButton!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.errorLabel.text = nil
        self.enableSubmitButton(true)

        // Do any additional setup after loading the view.
    }
    
    @IBAction private func submitTapped() {
        self.errorLabel.text = nil
        self.enableSubmitButton(false)

        guard let phone = self.emailTextField.text else {
          self.errorLabel.text = "Введите номер телефона."
          self.enableSubmitButton(true)
          return
        }

        guard self.validate(phone: phone) else {
          self.errorLabel.text = "Заполните валидный телефон."
          self.enableSubmitButton(true)
          return
        }
        
        Network.shared.apollo.perform(mutation: LoginMutation(phone: phone, password:passwordTextField.text ?? "")) { [weak self] result in
         defer {
           self?.enableSubmitButton(true)
         }

         switch result {
         case .success(let graphQLResult):
           if let token = graphQLResult.data?.login {
             let keychain = KeychainSwift()
             keychain.set(token, forKey: LoginViewController.loginKeychainKey)
             self?.dismiss(animated: true)
           }

           if let errors = graphQLResult.errors {
             print("Errors from server: \(errors)")
           }
         case .failure(let error):
           print("Error: \(error)")
         }
       }
        
        
    }
    
    @IBAction private func cancelTapped() {
      self.dismiss(animated: true)
    }
    
    private func enableSubmitButton(_ isEnabled: Bool) {
      self.submitButton.isEnabled = isEnabled
      if isEnabled {
        self.submitButton.setTitle("Submit", for: .normal)
      } else {
        self.submitButton.setTitle("Submitting...", for: .normal)
      }
    }
    
    private func validate(phone: String) -> Bool {
      //return email.contains("@")
        return true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
